Welcome to the DevOps Bootcamp project! 
This repository contains code and resources from a comprehensive DevOps bootcamp covering various technologies including Terraform, Kubernetes, Ansible, and Jenkins CI/CD.

Projects:
1. Terraform Project
This project includes code and resources related to Terraform, a tool for building, changing, and versioning infrastructure safely and efficiently.
2. Kubernetes Project
In this project, you will find code and resources for Kubernetes, an open-source container orchestration platform.
3. Ansible Project
The Ansible project contains code and resources for Ansible, a simple, yet powerful IT automation tool.
4. Jenkins CI/CD Project
This project includes code and resources for Jenkins, an open-source automation server that enables developers to build, test, and deploy their software.

Getting Started:
To get started with any of the projects, please refer to the respective project's README file for detailed instructions on how to set up and use the code.

Contributors:
Linh Hoang

Reference Link(DevOps Bootcamp): https://www.techworld-with-nana.com/devops-bootcamp